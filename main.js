var inputNumber = $('#inputNumber');
var ouncesMsg = $('#ounces-result');
var gramsMsg = $('#grams-result');
var kgMsg = $('#kg-result');

inputNumber.on('change', function () {
    ounces = inputNumber.val() * 16.000;
    grams = inputNumber.val() / 0.0022046;
    kilograms = inputNumber.val() / 2.2046;

    ouncesMsg.text(ounces);
    gramsMsg.text(grams);
    kgMsg.text(kilograms);
});